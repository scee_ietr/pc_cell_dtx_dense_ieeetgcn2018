%   N17BS_DRAW_HEXAGON.m creates and shows a network an hexagonal cellular network
%   composed of 17 base stations.

%   Other m-files required: channelgain_Winner.m 

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is part of the code used for the simulations done in the article:
%   R. Bonnefoi, C. Moy and J. Palicot, "Power Control and Cell 
%   Discontinuous Transmission used as a means of decreasing Small-Cell 
%   Networks’ Energy Consumption," in IEEE Transactions on Green
%   Communications and Networking. doi: 10.1109/TGCN.2018.2838759 

%   Author  : Rémi BONNEFOI
%   SCEE Research Team - CentraleSupélec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/17/2018


clear all;
close all;
clc;
rng('shuffle');

% Display base stations

% Base stations
N_BS                = 17;

x_bs                = zeros(1,N_BS);
y_bs                = zeros(1,N_BS);

for i=1:1:9,
    x_bs(1,i)       = 500*sqrt(3)*mod(i-1,3);
    y_bs(1,i)       = 250+500*floor((i-1)/3);
end

for i=10:1:17,
    x_bs(1,i)       = (sqrt(3)/2)*(500+1000*mod(i-10,2));
    y_bs(1,i)       = 500*floor((i-10)/2);
end

% distance in meters
xmin                = min(x_bs);
xmax                = max(x_bs);

ymin                = min(y_bs);
ymax                = max(y_bs);

% Draw hexagons
scale               = 500/sqrt(3);
N_sides             = 6;
t                   =(1/N_sides:1/N_sides:1)*2*pi;
y                   =sin(t);
x                   =cos(t);
x                   =scale*[x x(1)];
y                   =scale*[y y(1)];
%plot(x, y);
x_hexa      = zeros(1,N_BS*length(x));
y_hexa      = zeros(1,N_BS*length(y));

for i=1:1:N_BS,
    x_hexa(1,(i-1)*length(x)+1:i*length(x))         = x + x_bs(1,i);
    y_hexa(1,(i-1)*length(y)+1:i*length(y))         = y + y_bs(1,i);
end
% Users
Lambda           = 20*2.598;   % Average number of users in the cells converage
% Generating users
Nu                  = 0;

while Nu == 0,
    Nu                  = poissrnd(Lambda);
end

% Users coordinates
x_coord             = xmin+ (xmax-xmin)*rand(1,Nu);
y_coord             = ymin+(ymax-ymin)*rand(1,Nu);


colour = get(gca,'colororder');

% Display
%figure;
hold on;
scatter(x_bs,y_bs,'filled','LineWidth',7);
scatter(x_coord,y_coord,'+','LineWidth',2);
for i =1:1:N_BS,
    plot(x_hexa(1,(i-1)*length(x)+1:i*length(x)), y_hexa(1,(i-1)*length(x)+1:i*length(x)),'k-.','Linewidth',1);
end
legend('Base stations','Users');
grid on;
box on;
xlim([0 1732])
ylim([0 1500])
xlabel('Distance (m)')
ylabel('Distance (m)')
set(findall(gcf,'-property','FontSize'),'FontSize',12)