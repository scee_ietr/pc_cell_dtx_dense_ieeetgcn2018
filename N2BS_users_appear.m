%   N2BS_USERS_APPEAR.m simulates the behavior of the network when users appear and
%   in a network made of two base stations when Cell DTx and power control are used
%   so as to provide each user a given capacity while minimizing the base stations
%   energy consumption.

%   Other m-files required: channelgain_Winner.m 

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is part of the code used for the simulations done in the article:
%   R. Bonnefoi, C. Moy and J. Palicot, "Power Control and Cell 
%   Discontinuous Transmission used as a means of decreasing Small-Cell 
%   Networks’ Energy Consumption," in IEEE Transactions on Green
%   Communications and Networking. doi: 10.1109/TGCN.2018.2838759 

%   Author  : Rémi BONNEFOI
%   SCEE Research Team - CentraleSupélec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/17/2018

% cleaning
%%%%%%%%%%

clear all;
close all;
clc;
rng('shuffle');
% Parameters 
%%%%%%%%%%%%%%

% Number of frames between the appearence of the new users
Nb_frame 			= 10;
% Number of users
Nu                  = 10;

% Number of temporal slots 

% In a first time users just appear
Nb_slots            = Nb_frame*Nu;

% Power parameters
P0                  = 56000;
Ps                  = 39000;
m                   = 2.6;
Pmax                = 6300;

Coef_BS             = (P0-Ps)/m;

% Bandwidth
B                   = 10*10^6;

% carrier frequency
f                   = 2000;



% distance in meters
xmin                = 0;
xmax                = 1000;

ymin                = 0;
ymax                = 500;

% Base station positions
x_bs1               = 250;
y_bs1               = 250;

x_bs2               = 750;
y_bs2               = 250;

% capacity constraint
C                   = 5*10^6;

% distance to base station
x_coord             = xmin+ (xmax-xmin)*rand(1,Nu);
y_coord             = ymin+(ymax-ymin)*rand(1,Nu);

dBS1                = sqrt((x_coord-x_bs1).^2+(y_coord-y_bs1).^2);
dBS2                = sqrt((x_coord-x_bs2).^2+(y_coord-y_bs2).^2);

% Pathloss computations
[PL1_dB]            = channelgain_Winner('B1',dBS1,f);
[PL2_dB]            = channelgain_Winner('B1',dBS2,f);
    % Linear_gain
G1                  = 10.^(-PL1_dB/10);
G2                  = 10.^(-PL2_dB/10);
% BS selection
BS                  = 1+(min(PL1_dB,PL2_dB)==PL2_dB);

% Noise
N_dB                = -174+10*log10(B);
N                   = 10^(N_dB/10);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot users
x_coord1                 = x_coord(BS==1);
y_coord1                 = y_coord(BS==1);
x_coord2                 = x_coord(BS==2);
y_coord2                 = y_coord(BS==2);

% Display users
figure;
hold on;
grid on;
box on;
scatter(x_bs1,y_bs1,'filled');
scatter(x_bs2,y_bs2,'filled');
scatter(x_coord1,y_coord1,'+');
scatter(x_coord2,y_coord2,'+');
xlim([0,1000]);
ylim([0,500]);
legend('Base station 1', 'Base station 2','Users served by BS1', 'Users served by BS2')
set(findall(gcf,'type','axes'),'fontsize',13)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% We now suppose that the users appear one after the other in the cell
% coverage

% Computation of the transmit power and user service time for the first user
    mu                      = (C*log(2)/B)*(1/(lambertw(exp(-1)*(max(G1(1,1),G2(1,1))*Coef_BS/N-1))+1));
    tr_pow                  = (N/max(G1(1,1),G2(1,1)))*(2^(C/(B*mu))-1);
    
    % Save power and time
    pow                             = zeros(Nu,1+2*(Nu-1)*Nb_frame);
    time                            = zeros(Nu,1+2*(Nu-1)*Nb_frame);
    
    pow(1,1)                        = tr_pow;
    time(1,1)                       = mu;
    
% Bring up people
for i=1:1:Nu-1
    
    % Select the users
    G1_temp                         = G1(1,1:i+1);
    G2_temp                         = G2(1,1:i+1);
    BS_temp                         = BS(1,1:i+1);
    
    mu                              = [mu 0];
    tr_pow                          = [tr_pow, 0];
    
    
    for j=1:1:Nb_frame
        
        % Computation of interference at this frame
        
        % Interference generated by BS1 (without the loss)
        I_gen1                                      = sum(mu(BS_temp==1).*tr_pow(BS_temp==1));
        % Interference generated by BS2 (without the loss)
        I_gen2                                      = sum(mu(BS_temp==2).*tr_pow(BS_temp==2));
        
        % Channel and interference coefficients
        GNplusI_1                                   = G1_temp(BS_temp==1)./(N+I_gen2*G2_temp(BS_temp==1));
        GNplusI_2                                   = G2_temp(BS_temp==2)./(N+I_gen1*G1_temp(BS_temp==2));
        
        % users minimum service time
        mu_min1                                     = C./(B*log2(1+Pmax*GNplusI_1));
        mu_min2                                     = C./(B*log2(1+Pmax*GNplusI_2));
        
        if (sum(mu_min1)<1) && (sum(mu_min2)<1),
            % Computation of users service time of the first user
            

            mu(BS_temp==1)                              = max((C*log(2)/B)*(1./(lambertw(exp(-1)*(Coef_BS*GNplusI_1-1))+1)),mu_min1);

            if sum(mu(BS_temp==1))>1,
                [mu(BS_temp==1),nb_iter]                = opt_sol_Newton(sum(BS_temp==1),mu(BS_temp==1),mu_min1,B,C,GNplusI_1*B,m,Pmax,P0,Ps,0.001);
                disp('OK')
            end

            % Second base station
            

            mu(BS_temp==2)                            	= max((C*log(2)/B)*(1./(lambertw(exp(-1)*(Coef_BS*GNplusI_2-1))+1)),mu_min2);

            if sum(mu(BS_temp==2))>1,
                [mu(BS_temp==2),nb_iter]                = opt_sol_Newton(sum(BS_temp==2),mu(BS_temp==2),mu_min2,B,C,GNplusI_2*B,m,Pmax,P0,Ps,0.001);
                disp('OK')
            end

            % Computation of the transmit powers
            tr_pow(BS_temp==1)                      	= ((N+I_gen2*G2_temp(BS_temp==1))./(G1_temp(BS_temp==1))).*(2.^(C./(B*mu(BS_temp==1)))-1);
            tr_pow(BS_temp==2)                      	= ((N+I_gen1*G1_temp(BS_temp==2))./(G2_temp(BS_temp==2))).*(2.^(C./(B*mu(BS_temp==2)))-1);

            % Save results
            pow(1:length(mu),(i-1)*Nb_frame+j+1)        = transpose(tr_pow);
            time(1:length(mu),(i-1)*Nb_frame+j+1)       = transpose(mu);
        else
           disp('Impossible to serve the users, too large capacity constraint') 
        end
    end
    
    
end

% make people disappear
for i=1:1:Nu-1
    
    % Select the users
    G1_temp                         = G1(1,1:Nu-i);
    G2_temp                         = G2(1,1:Nu-i);
    BS_temp                         = BS(1,1:1:Nu-i);
    
    mu(1,Nu+1-i)                    = 0;
    tr_pow(1,Nu+1-i)                = 0;
    
    
    for j=1:1:Nb_frame
        
        % Computation of interference at this frame
        
        % Interference generated by BS1 (without the loss)
        I_gen1                                      = sum(mu(BS_temp==1).*tr_pow(BS_temp==1));
        % Interference generated by BS2 (without the loss)
        I_gen2                                      = sum(mu(BS_temp==2).*tr_pow(BS_temp==2));
        
        % Channel and interference coefficients
        GNplusI_1                                   = G1_temp(BS_temp==1)./(N+I_gen2*G2_temp(BS_temp==1));
        GNplusI_2                                   = G2_temp(BS_temp==2)./(N+I_gen1*G1_temp(BS_temp==2));
        
        % users minimum service time
        mu_min1                                     = C./(B*log2(1+Pmax*GNplusI_1));
        mu_min2                                     = C./(B*log2(1+Pmax*GNplusI_2));
        
        if (sum(mu_min1)<1) && (sum(mu_min2)<1),
            % Computation of users service time of the first user
            

            mu(BS_temp==1)                              = max((C*log(2)/B)*(1./(lambertw(exp(-1)*(Coef_BS*GNplusI_1-1))+1)),mu_min1);

            if sum(mu(BS_temp==1))>1,
                [mu(BS_temp==1),nb_iter]                = opt_sol_Newton(sum(BS_temp==1),mu(BS_temp==1),mu_min1,B,C,GNplusI_1*B,m,Pmax,P0,Ps,0.001);
                disp('OK')
            end

            % Second base station
            

            mu(BS_temp==2)                            	= max((C*log(2)/B)*(1./(lambertw(exp(-1)*(Coef_BS*GNplusI_2-1))+1)),mu_min2);

            if sum(mu(BS_temp==2))>1,
                [mu(BS_temp==2),nb_iter]                = opt_sol_Newton(sum(BS_temp==2),mu(BS_temp==2),mu_min2,B,C,GNplusI_2*B,m,Pmax,P0,Ps,0.001);
                disp('OK')
            end

            % Computation of the transmit powers
            tr_pow(BS_temp==1)                      	= ((N+I_gen2*G2_temp(BS_temp==1))./(G1_temp(BS_temp==1))).*(2.^(C./(B*mu(BS_temp==1)))-1);
            tr_pow(BS_temp==2)                      	= ((N+I_gen1*G1_temp(BS_temp==2))./(G2_temp(BS_temp==2))).*(2.^(C./(B*mu(BS_temp==2)))-1);

            % Save results
            pow(1:length(mu),(Nu-1)*Nb_frame+(i-1)*Nb_frame+j+1)        = transpose(tr_pow);
            time(1:length(mu),(Nu-1)*Nb_frame+(i-1)*Nb_frame+j+1)       = transpose(mu);
        else
           disp('Impossible to serve the users, too large capacity constraint') 
        end
    end
    
    
end

for i=1:2:5
    legendvalue{floor(i/2)+1}           = ['User ' num2str(i)];
end

colour          = get(gca,'colororder');

pow_plot        = transpose(pow); 
time_plot       = transpose(time);
figure;
hold on;
grid on;
box on;
plot(pow_plot(:,1)/1000,'-','linewidth',2,'Color',colour(1,:))
plot(pow_plot(:,3)/1000,'--','linewidth',2,'Color',colour(4,:))
plot(pow_plot(:,5)/1000,':','linewidth',2,'Color',colour(5,:))
legend(legendvalue)
ylabel('Users^{\prime} transmit power (W)');
xlabel('Frame index')
xlim([1, 1+2*(Nu-1)*Nb_frame]);
set(findall(gcf,'type','axes'),'fontsize',13)

figure;
hold on;
grid on;
box on;
box on;
plot(time_plot(:,1),'-','linewidth',2,'Color',colour(1,:))
plot(time_plot(:,3),'--','linewidth',2,'Color',colour(4,:))
plot(time_plot(:,5),':','linewidth',2,'Color',colour(5,:))
legend(legendvalue)
ylabel('Users^{\prime} service time')
xlabel('Frame index')
xlim([1, 1+2*(Nu-1)*Nb_frame]);
set(findall(gcf,'type','axes'),'fontsize',13)