### PC_Cell_DTx_Dense_IEEETGCN2018

This repository contains part of the matlab code which has been used in order to generate the simulation results done for our article: 

R. Bonnefoi, C. Moy and J. Palicot, "Power Control and Cell  Discontinuous Transmission used as a means of decreasing Small-Cell  Networks� Energy Consumption," in *IEEE Transactions on Green Communications and Networking*. doi: 10.1109/TGCN.2018.2838759

This article can be found on IEEExplore. A version is also available at: https://hal.archives-ouvertes.fr/hal-01798963

### Short description 

In this repository, we run simulations in two different networks.

* A first network made of two base stations (the code forN2BS)

* A second network made of 17 base stations (N17BS). This second network can be viewed below:

  ![N17BS](network.jpg)



Each file starts by the id of the network simulated in this file (which can be either N2BS or N17BS). We have he following files:

* (id network)_users_appear.m simulates the iterative behavior of base stations when a new user appears in the network.
* (id network)_reduced_maxpow.m provides the average base station power consumption as a function of the users' capacity constraint.
* (id network)_optim_maxpow.m provides the average base station power consumption as a function of the maximum transmit power of the base station.
* N17BS_draw_hexagon.m generates the network made of 17 base stations.
* Other files are matlab functions which are used in the files listed above.

### License

[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).