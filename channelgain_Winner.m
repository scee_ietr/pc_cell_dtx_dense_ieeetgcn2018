function [PL]    = channelgain_Winner(model,distance,frequency)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function computes the channel gain (dB) for one user with
% the winner 2 channel model and the B1 scenario
%
% parameters:
% model         : Path loss model, can be 'B1', 'B2','B3','B5a', 'B5c',
% 'B5f', 'C1','C2','C3' and 'D1'
% distance      : distances between the base station and the mobiles (meters)
% frequency     : carrier frequency(Hz)
%
% outputs:
% PL            : pathloss in dB
%
% Author : R. Bonnefoi
%          PhD student SCEE research team
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% parameters
c                   = 3*10^8;

% computation of the LOS probability
switch model
    case {'B1'}
        hbs                 = 10;
        hms                 = 1.5;
        streetwidth         = 20;
        % probability of LOS:
        Plos                = min(18./distance,1).*(1-exp(-distance/36))+exp(-distance/36);
        prob                = unifrnd(0,1,1,length(distance));
        % choose between LOS and NLOS
        LOS                 = prob<Plos;
        
    case {'B2'}
        hbs                 = 10;
        hms                 = 1.5;
        streetwidth         = 20;
        LOS                 = false(1,length(distance));
    case {'B3'}
        d_inf_10            = distance<10;
        Plos                = exp(-(distance-10)/45);
        prob                = unifrnd(0,1,1,length(distance));
        LOS                 = (prob<Plos)|d_inf_10;
    case{'B5a','B5f'}
    case{'B5c'}
        hbs                 = 10;
        hms                 = 5;
        streetwidth         = 20;
        
        LOS                 = true(1,length(distance));
    case {'C1'}
        hbs                 = 25;
        hms                 = 1.5;
        Plos                = exp(-(distance/200));
        prob                = unifrnd(0,1,1,length(distance));
        LOS                 = prob<Plos;
    case {'C2'}
        hbs                 = 25;
        hms                 = 1.5;
        % probability of LOS:
        Plos                = min(18./distance,1).*(1-exp(-distance/63))+exp(-distance/63);
        prob                = unifrnd(0,1,1,length(distance));
        % choose between LOS and NLOS
        LOS                 = prob<Plos;
    case {'C3'}
        hbs                 = 25;
        hms                 = 1.5;
        % probability of LOS:
        LOS                 = false(1,length(distance));
    case {'D1'}
        hbs                 = 32;
        hms                 = 1.5;
        % probability of LOS:
        Plos                = exp(-(distance/1000));
        prob                = unifrnd(0,1,1,length(distance));
        % choose between LOS and NLOS
        LOS                 = prob<Plos;
    otherwise
        error('Channel model unknown or not implemented');       
end

% computation of the pathloss
switch model
    case {'B1','B2','B5c'}
        d1                  = ones(1, length(distance));
        while (d1<10),
            d2                      = distance.*rand(1,length(distance))+10+ streetwidth/2;
            d1                      = sqrt(abs(distance.^2 - d2.^2));
        end
        d1(LOS)             = distance(LOS);

        % Compute path loss
        % LOS case
        d_bp                = 4*(hbs-1)*(hms-1)*frequency/c;
        supd_bp             = d1>d_bp;

        PLlos               = 22.7*log10(d1)+41+20*log10(frequency/(5*10^9));
        PLlos(supd_bp)      = 40*log10(d1(supd_bp))+9.45-17.3*log10(hbs-1)-17.3*log10(hms-1)+2.7*log10(frequency/(5*10^9));

        n1                  = max(2.8-0.0024*d1,1.84);
        n2                  = max(2.8-0.0024*d2,1.84);

        PL                  = zeros(1,length(distance));

        % shadowing
        Sh                  = normrnd(0,4,1,length(distance));
        Sh(LOS)             = normrnd(0,3,1,length(distance(LOS)));
        for i =1:1:length(distance)  
            if LOS(1,i),
                PL(1,i)                 = PLlos(1,i);
            else
                % NLOS case
                if d2(1,i)<d_bp,
                    PLlos2              = 22.7*log10(d2(1,i))+41+20*log10(frequency/(5*10^9));
                else
                    PLlos2              = 40*log10(d2(1,i))+9.45-17.3*log10(hbs-1)-17.3*log10(hms-1)+2.7*log10(frequency/(5*10^9));
                end
    

                PL1                 = PLlos(1,i) +20-12.5*n1(1,i)+10*n1(1,i)*log10(d2(1,i))+3*log10(frequency/(5*10^9));
                PL2                 = PLlos2 +20-12.5*n2(1,i)+10*n2(1,i)*log10(d1(1,i))+3*log10(frequency/(5*10^9));
    
                PL(1,i)             = min(PL1,PL2);
            end
        end
        
        
    case {'B3'}
        A_LOS               = 13.9;
        B_LOS               = 64.4;
        C_LOS               = 20;
        A_NLOS              = 37.8;
        B_NLOS              = 36.5;
        C_NLOS              = 23;
        
        PL                  = A_NLOS*log10(distance)+B_NLOS+C_NLOS*log10(frequency/(5*10^9));
        PL(LOS)             = A_LOS*log10(distance(LOS))+B_LOS+C_LOS*log10(frequency/(5*10^9));
        
        Sh                  = normrnd(0,4,1,length(distance));
        Sh(LOS)             = normrnd(0,3,1,length(distance(LOS)));
    case{'B5a'}
        A_LOS               = 23.5;
        B_LOS               = 42.5;
        C_LOS               = 20;
        PL                  = A_LOS*log10(distance)+B_LOS+C_LOS*log10(frequency/(5*10^9));
        Sh                  = normrnd(0,4,1,length(distance));
    case{'B5f'}
        A_NLOS              = 23.5;
        B_NLOS              = 42.5;
        C_NLOS              = 20;
        PL                  = A_NLOS*log10(distance)+B_NLOS+C_NLOS*log10(frequency/(5*10^9));
        Sh                  = normrnd(0,8,1,length(distance));
    case{'C1'}
        d_bp                = 4*(hbs)*(hms)*frequency/c;
        supd_bp             = distance>d_bp;
        
        % Pathloss
        PL                  = (44.9-6.55*log10(hbs))*log10(distance)+31.46+5.83*log10(hbs)+23*log10(frequency/(5*10^9));
        PL(LOS)             = 23.8*log10(distance(LOS))+41.2+20*log10(frequency/(5*10^9));
        PL(LOS & supd_bp)   = 40*log10(distance(LOS & supd_bp))+11.65-16.2*log10(hbs*hms)+3.8*log10(frequency/(5*10^9));
        
        Sh                  = normrnd(0,8,1,length(distance));
        Sh(LOS)             = normrnd(0,4,1,length(distance(LOS)));
        Sh(LOS & supd_bp)   = normrnd(0,6,1,length(distance(LOS & supd_bp)));
    case {'C2','C3'}
        d_bp                = 4*(hbs-1)*(hms-1)*frequency/c;
        supd_bp             = distance>d_bp;
        
        % Pathloss
        PL                  = (44.9-6.55*log10(hbs))*log10(distance)+34.46+5.83*log10(hbs)+23*log10(frequency/(5*10^9));
        PL(LOS)             = 26*log10(distance(LOS))+39+20*log10(frequency/(5*10^9));
        PL(LOS & supd_bp)   = 40*log10(distance(LOS & supd_bp))+13.47-14*log10(hbs*hms)+6*log10(frequency/(5*10^9));
        
        Sh                  = normrnd(0,8,1,length(distance));
        Sh(LOS)             = normrnd(0,4,1,length(distance(LOS)));
        Sh(LOS & supd_bp)   = normrnd(0,6,1,length(distance(LOS & supd_bp)));
    case{'D1'}
        d_bp                = 4*(hbs)*(hms)*frequency/c;
        supd_bp             = distance>d_bp;
        
        % Pathloss
        PL                  = 25.1*log10(distance)+55.4-0.13*log10(hbs-25)*log10(distance/100)+21.3*log10(frequency/(5*10^9));
        PL(LOS)             = 21.5*log10(distance(LOS))+44.2+20*log10(frequency/(5*10^9));
        PL(LOS & supd_bp)   = 40*log10(distance(LOS & supd_bp))+10.5-18.5*log10(hbs*hms)+1.5*log10(frequency/(5*10^9));
        
        Sh                  = normrnd(0,8,1,length(distance));
        Sh(LOS)             = normrnd(0,4,1,length(distance(LOS)));
        Sh(LOS & supd_bp)   = normrnd(0,6,1,length(distance(LOS & supd_bp)));
    otherwise
        error('Channel model unknown or not implemented');
end

PL                  = PL+Sh;

end