function [c,ceq]            = constraint_generation(BS,G1,G2,N,C,B,Pmax,mu)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function generates the constraint on the maximum transmit power.
%
% Inputs:
%       BS                  : Base Station assigned to each user
%       G1                  : Path loss (linear) between base station 1 and
%                             user
%       G2                  : Path loss (linear) between base station 2 and
%                             user
%       N                   : Thermal noise
%       C                   : Users capacity constraints
%       B                   : Total bandwidth
%       Pmax                : Maximum RF transmit power
%       mu                  : temporal variables (symbolic)
%
% Outputs :
%
%       c                   : Constraint on the maximum transmit power
%
% Author : R. Bonnefoi
%          PhD student SCEE research team
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % maximum and minimum value of G for each user
    G_max               = max(G1,G2);
    G_min               = min(G1,G2);
    
    % generate alpha_k and beta_k
    
    beta                = (N./G_max).*mu.*((2.^(C./(B*mu)))-1);
    alpha               = G_min.*beta/N;

    alpha_1             = alpha(BS==1);
    alpha_2             = alpha(BS==2);
    beta_1              = beta(BS==1);  
    beta_2              = beta(BS==2); 
    
    % Useful sums and product for the computation of x_k
    S_alpha1            = sum(alpha_1);
    S_alpha2            = sum(alpha_2);
    S_beta1             = sum(beta_1);
    S_beta2             = sum(beta_2);
    
    % sum of alpha_i alpha_j
    S_alphaalpha        = S_alpha1*S_alpha2;
    S_a1b2              = S_alpha1*S_beta2;
    S_a2b1              = S_alpha2*S_beta1;
    
    c                   = zeros(length(mu),1);
    for k=1:1:length(mu)
        if S_alphaalpha>1,
            c           = NaN(length(mu),1);
        else
            if BS(1,k)==1,
                c(k,1)              = beta(1,k)+alpha(1,k)*((S_beta2+S_a2b1)/(1-S_alphaalpha))-mu(1,k)*Pmax;
            else
                c(k,1)              = beta(1,k)+alpha(1,k)*((S_beta1+S_a1b2)/(1-S_alphaalpha))-mu(1,k)*Pmax;
            end
        end
    end
    
    %c(length(mu)+1,1)           = S_alphaalpha-0.99;
    ceq                         = [];
end