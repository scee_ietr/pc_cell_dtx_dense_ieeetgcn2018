function [obj_fun]            = objective_function(BS,G1,G2,N,C,B,P0,Ps,m,mu)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function generates the objective function of base station energy consumption minimization.
%
% Inputs:
%       BS                  : Base Station assigned to each user
%       G1                  : Path loss (linear) between base station 1 and
%                             user
%       G2                  : Path loss (linear) between base station 2 and
%                             user
%       N                   : Thermal noise
%       C                   : Users capacity constraints
%       B                   : Total bandwidth
%       P0                  : base station static power consumption
%       Ps                  : Base station power consumption during sleep
%                             mode
%       m                   : coefficient of the load dependance
%       mu                  : temporal variables (symbolic)
%
% Outputs :
%
%       obj_fun             : Objective function
%
% Author : R. Bonnefoi
%          PhD student SCEE research team
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % maximum and minimum value of G for each user
    G_max               = max(G1,G2);
    G_min               = min(G1,G2);
    
    % generate alpha_k and beta_k
    
    beta                = (N./G_max).*mu.*((2.^(C./(B*mu)))-1);
    alpha               = G_min.*beta/N;

    alpha_1             = alpha(BS==1);
    alpha_2             = alpha(BS==2);
    beta_1              = beta(BS==1);  
    beta_2              = beta(BS==2); 
    
    % Useful sums and product for the computation of x_k
    S_alpha1            = sum(alpha_1);
    S_alpha2            = sum(alpha_2);
    S_beta1             = sum(beta_1);
    S_beta2             = sum(beta_2);
    
    % sum of alpha_i alpha_j
    S_alphaalpha        = S_alpha1*S_alpha2;
    S_a1b2              = S_alpha1*S_beta2;
    S_a2b1              = S_alpha2*S_beta1;
    
    obj_fun             =2*Ps+(P0-Ps)*sum(mu)+m*((S_beta1+S_beta2+S_a1b2+S_a2b1)/(1-S_alphaalpha));
    
end